#### Installing Jenkins

##### Manual Installation

-   Add the repository key to system

```bash
$ wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
OK # system response
```

-   Add below entry to /etc/apt/sources.list

```bash
deb https://pkg.jenkins.io/debian-stable binary/
```

Run following commands-

```bash
$ sudo apt update
$ sudo apt install jenkins
# jenkins is installed as a service which can be controlled by systemctl
```

##### Install with Docker

-   Pull the Image
    ```Docker
    docker pull jenkins/jenkins
    ```
-   Run the container

    ```Docker
    docker run -d -p 8080:8080 50000:50000 jenkins/jenkins:lts
    ```

-   Run container with volume mounting for data persistance

    ```Docker
    docker run -d --name jenkins -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts
    ```

    `-d` is for detached mode.

#### Setup Jenkins

On initial access of jenkis server (ip:8080), the startup asks for an administrator password which can be obtained from following file

```path

/var/jenkins_home/secrets/initialAdminPassword # this is path inside the docker container
/var/lib/docker/volumes/jenkins_home/_data/secrets/initialAdminPassword # this is the path on docker host if container was created with explicit volumme mounting

```

#### Jenkins Distributed Architecture

Consists of a Jenkins Master and one or more Jenkins Slave instances.

Master schedules the build jobs, dispatching the jobs to slaves for execution and monitors the slaves. Jobs can also be executed on Master instance itself.

-   ##### Adding slave instances
    Manage Jenkins > Manage Nodes > New Node
    Select Permanent Node, provide access details (IP and Authentication)
    After adding, jenkins creates a slave.jar file in provided remote root directory.
