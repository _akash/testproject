#### Docker Swarm

Multiple docker hosts can be combined in a cluster to manage/distribute the services to maintain high availability/load balance the servers/hardware.

In docker swarm, one docker host is the swarm manager and other hosts/nodes are worker nodes.

-   ##### Initializing the swarm -

    A host can be designated as swarm manager with following command -

    ```
    $ docker swarm init
    Swarm initialized: current node (7tryjfu0rfznfrn8nbrd5s3e8) is now a manager.

    To add a worker to this swarm, run the following command:

        docker swarm join --token SWMTKN-1-58vq5pwdg36tq5o5f9d43jcnvot8zv3qgn50b0ownlr8fukbz1-aakfoa09zqs8cp02c2litybjs 78.46.164.4:2377

    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
    ```

    As seen in the output of above command, the node is designated as manager node.

    It also provides the command to run on other docker hosts to join the swarm as worker nodes.

Docker swarm ochestrator can be utilized to run the multiple instances of containers on multiple nodes.
The following command can be used to run a container across the nodes-

```bash
$ docker service create --replicas=3 image-name
```
