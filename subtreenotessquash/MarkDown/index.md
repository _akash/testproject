Markdown is lightweight markup language with plain text formatting, no tags necessary.

Escape character is a backslash (`\`)

Comments can be like html style

```md
<!-- Comment -->
```
Headings can be applied with adding # symbols

```
# Heading 1
##### Heading 4
```

# Heading 1

#### Heading 4

Text can be made italic by using underscores ( \_ ) before and after the wordor by using asterics

```
_italics_
*italics*
```

_italics_

Strong/Bold text can be done by using two underscores/asterics before and after the text-

```
___bold text___
***bold text***
```

**bold text**

Strikethrough text can be done using two tilde symbols
```
~~strikethrough~~
```

~~strikethrough~~

Horizontal rule/line separator can be done by using triple hyphens or underscores.

```
___
---
```

---

Blockquotes can be added by using greater than symbol
```
>Blockquote
```

> Blockquote

Links can be added with a text using following format

A space is needed between url and Title text to display on hover. Without a title, the url is diplayed on hover.
```
[Link Text](Link Url Title_on_Hover)
```

[Markdown Notes](# 'Markdown Notes')


Unordered lists can be done with \* or \- at the beginning of line

```
* Item A
- Item D
* Item B
    * Item B.C
```

-   Item A
-   Item D
-   Item B
    -   Item B.C

Nested Items can be added with a tab before item

Ordered list can be done with adding 1. at the beginning of line and markdown will autoincrement the number

```
1. Item 1
1. Item 2
1. Item 3
```

1. Item 1
1. Item 2
1. Item 3

#### Gitlab/Github specific markdown -

Inline code blocks can be added with a backtick \` code block`

`<p> A paragraph in html </p>`

Multiline code blocks can be added with ``` or ~~~ symbols

```bash
git add .
git commit -m "commit message"
```

Code language can be specified along with first set of backticks - ` ```bash `.

This adds language specific syntax highlighting as seen above in git commands

Images can be added similar to links, by putting ! in front of the square brackets.
```
![Image Name](Image Location)
```

![Gitlab logo](https://about.gitlab.com/images/press/logo/png/gitlab-logo-white-rgb.png)

Additionally tasklists and tables can also be added

#### Task lists

```
- [x] Completed Task
- [x] Another completed Task
- [ ] Incomplete Task
    - [ ] Incomplete subtask 1
    - [x] Complete subtask 2
1. [x] Completed Task
1. [ ] Incomplete Task
```

-   [x] Completed Task
-   [x] Another completed Task
-   [ ] Incomplete Task
    -   [ ] Incomplete subtask 1
    -   [x] Complete subtask 2

1. [x] Completed Task 3
1. [ ] Incomplete Task 2

The above code is rendered as shown on github/gitlab -

![checklist](./images/tasklist.png "task list rendering")

#### Tables

```
| Column 1    | Column 2 |
| ----------- | -------- |
| Item 1      | Item 2   |
| Second line | foo      |
| Third line  | bar      |
```

| Column 1    | Column 2 |
| ----------- | -------- |
| Item 1      | Item 2   |
| Second line | foo      |
| Third line  | bar      |

**For complex tables, html should be used**
