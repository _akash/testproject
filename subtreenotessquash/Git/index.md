## Git

Git is a free and open source distributed version control system. The official documentation can be found at [Git SCM](https://git-scm.com/docs/ 'Git SCM')

#### Installing Git

-   ##### On Windows

    Git can be installed on windows by downloading and running the Git installer from the [Git - Downloading Page](https://git-scm.com/download/win 'Git - Downloading Package') from git-scm.

-   ##### On Linux

    Git can be installed on linux systems using the Linux distribution's package manager.

    For e.g. on Debian/Ubuntu git can be installed by running following command

    ```bash
    $ apt-get install git
    ```

    For ubuntu, to get latest stable upstream version can be installed with following commands-

    ```bash
    $ add-apt-repository ppa:git-core/ppa
    $ apt update
    $ apt install git
    ```

    More distribution specific install instructions are available at [git-scm](https://git-scm.com/download/linux 'Git Linux Installation')

    ##### Verify Installation

    Git installation can be verified by running the following command

    ```bash
    $ git --version
    'git version 2.20.1.windows.1'
    ```

#### Git File States

There are three states that files in a Git Repository reside in

-   **Commited** - The data is safely stored in git.
-   **Modified** - The file is changed but not commited.
-   **Staged** - The file is marked as modified in its current version and will be commited with next commit.

Git has three main sections -

-   **The working tree** - The working tree is a single checkout of one version of the Project. Files in this tree can be modified directly. The files in this section are either tracked or untracked.
-   **The Staging Area** - The staging area is a file (the Index) that stores information about what will go into next commit.
-   **Git Directory (.git directory)** - This is where Git stores all commited files and metadata.

The general git flow is as below -

1. Files are modified in the working tree
1. Files are then selected and added to staging area.
1. A commit is performed, which takes the files from staging area and stores that snapshot in Git Directory. A hash is generated called commit id which can be used to perform additional operations.
1. Individual files/ commits are checked out from Git repository to working directory for modification/ recovery.

The following diagram shows the general flow -

![gitflow](./images/generalflow.png)


#### Git HEAD 

HEAD is a reference to the latest commit in the current (checked out) _branch_.

To see the current head, following command can be used-
```bash
$ cat .git/HEAD
ref: refs/heads/master
```

An exception to this is when a specific commit is checked out instead of branch, in such cases the HEAD is detached - 
```bash
$ git checkout 8109dfca4a69c467f06958b0da644be1b97a05f5
HEAD is now at 8109dfc Test Commit

$ git status
HEAD detached at 8109dfc
nothing to commit, working tree clean
```

Work done on detached HEAD can be carried over to new branch and merged back to master-
```bash
$ git status
HEAD detached at 8109dfc
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   test.txt


$ git commit -m "new file at detached head"
[detached HEAD 9ab7550] new file at detached head
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test.txt


 git branch <new-branch-name> 9ab7550
 ```

 A common use/example of detached HEAD is submodules. Submodules are usually checked out at a specific commit and do not always have the latest content from the remote repository.

#### Git Commands

-   ##### Initializing a git repository

    A new git repository can be initiated in a new folder or an existing project folder. The command used to initialize git repository is

    ```bash
    $ git init
    Initialized empty Git repository in D:/Projects/GitNotes/.git/
    ```

    A repository can be also initiated in a new folder under current folder.

    ```bash
    $ git init testrepo
    Initialized empty Git repository in D:/Projects/GitNotes/testrepo/.git/
    ```

    Running above command will create a testrepo folder in current directory and initialize a git repository in testrepo folder.

-   ##### Cloning an existing repository

    An existing repository can be cloned with following command

    ```bash
    $ git clone https://gitlab.com/_akash/notes.git
    Cloning into 'notes'...
    ```

    > clone command needs the location of git repository.

    A repository can be cloned by using ssh instead of https with following command

    ```bash
    $ git clone git@gitlab.com:_akash/notes.git
    Cloning into 'notes'...
    ```

    p.s. - Replace gitlab.com with domain of the service provider.

    A local repository can be cloned into a new folder with following command -

    ```bash
    $ git clone testrepo targetrepo # here testrepo is the source repo to be cloned
    Cloning into 'targetrepo'...
    $ ls
    targetrepo/  testrepo/
    ```

    If a repository is private, user credentials are asked before cloning.

    ```bash
    $ git clone https://gitlab.com/_akash/privatenotes.git
    Cloning into 'privatenotes'...
    Username for 'https://gitlab.com': _akash
    Password for 'https://_akash@gitlab.com':
    remote: HTTP Basic: Access denied
    fatal: Authentication failed for 'https://gitlab.com/_akash/privatenotes.git/'
    exit status 128
    ```

    This authentication may be required multiple times. This can be avoided by accessing the git service with ssh.

    The details for ssh configuration are present in [ssh](../ssh/ssh.md#accessing-remote-git-repo).

-   #### Git Configuration

-   List Configurations
    Current configurations can be listed with following command -
    ```sh
    $ git config --global --list
    user.name=Akash
    user.email=akashvibhute@live.com
    ```
-   Edit Configuration file
    The configuration file can be edited with following command -

    ```sh
    $ git config --global --edit
    ```

    The above command will open up the config file in a text editor on terminal (either vi / nano or any other configured editor). The editor can be specified in the configuration file.

    --global specified above is the scope of configuration. There are three scopes

    -   global - Scoped to User Account
    -   system - Scoped to the Machine/Computer
    -   local - Scoped to the current repository

    Usually changes are added to global config file. The file can also be manually edited. It is generally present in User folder in with .giconfig name


    The user name and email are important to set as it is the name/email used for commiter identification.
    These details are visible in git log.

    User Details can be set from the terminal with following commands -

    ```bash
    git config --global user.name "Akash"
    git config --global user.email "akashvibhute@live.com"
    ```

    The default editor can be configured with following command -

    ```bash
    git config --global core.editor  "'C:/Program Files/Notepad++/notepad++.exe'
    ```

-   #### Recording File Changes

-   Current Status - current status of the repository can be retrieved by following command -

    ```bash
    $ git status
    On branch master

    No commits yet

    Changes to be committed:
        (use "git rm --cached <file>..." to unstage)

            new file:   file1

    Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git checkout -- <file>..." to discard changes in working directory)

            modified:   file1

    Untracked files:
        (use "git add <file>..." to include in what will be committed)

            notes/

    nothing added to commit but untracked files present (use "git add" to track)
    ```

    The above snippet shows the different states of the files in the working directory.

    _The changes to be commmited_ section shows the file instances that have been staged and will be commited to Git Directory on next commit.
    _The changes not staged for commit_ section shows file instances which are tracked and are modified but not staged for commit.

    Note that file1 is present in both changes to be commited and changes not staged for commit sections. This is because it was staged and then modified. So with a commit, the earlier staged version will be commited and new changes will remain uncommited.


    _The Untracked files_ section lists files which are not staged and also not ignored.

    **Files and Directories can be ignored in Git by adding them to a .gitignore file in project root directory**

    The following diagram shows file life cycle -

    ![File Life Cycle](./images/lifecycle.png "File Life Cycle")

    Git also has an option for short status as the output of status command is quite verbose.
    ```bash
    $ git status -s
    M README
    MM markdown.md
    A git/git.md
    ?? LICENSE.txt
    ```
    | Short Status| Description                             |
    | ----------- | --------                                |
    | M           | Modified file                           |
    | MM          | Modified, Staged and Modified again     |
    | A           | Added to staging aread                  |
    | ??          | Untracked file                          |

-   Track Files
    ```bash
    $ git add testfile  # stage a file
    $ git add .         # stage new and modified files
    $ git add -A        # stage new, modified and delete files
    $ git add -u        # stage modified and deleted files
    ```

-   Commit Files

    A commit is a snapshot of project files at a given point. With a commit, staged files are moved to the object database in Git Directory.
    ```bash
    $ git commit -m "Initial Commit"
    [master (root-commit) 51de56b] Initial Commit
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 testfile
    ```
    The commit can have multiple -m options.
    commit command without commit message will open the text editor to add the message and description.

    The staging area can be skipped by adding -a option to the commit.
    ```bash
    $ git commit -am  "Commit Message" # or
    $ git commit -a -m "Commit Message"
    ```
-   Viewing Commit Logs

    Viewing commit logs is helpful to see what changes were made in past.
    Commit log can be viewed by following command-
    ```bash
    $ git log
    commit 25ea957c9cfd81791c21519cac21fba8d36c8089 (HEAD -> master)
    Author: Akash <akashvibhute@live.com>
    Date:   Wed Jan 1 02:49:42 2020 +0530

        Updated testfile

    commit 51de56bd96db0e422a831e28517de5f33eb4b8fe
    Author: Akash <akashvibhute@live.com>
    Date:   Wed Jan 1 02:24:05 2020 +0530

        Initial Commit
    ```

    The output of git log is similar to less command and can be exited with q.
    To show limited number of recent commit history add an option -n, with n being the number of records to be displayed.
    ```bash
    $ git log -2
    ```
    The path option ( -p or --patch ) shows the difference introduced in each commit.
    ```bash
    $ git log -p -1
    commit 25ea957c9cfd81791c21519cac21fba8d36c8089 (HEAD -> master)
    Author: Akash Vibhute <akashvibhute@live.com>
    Date:   Wed Jan 1 02:49:42 2020 +0530

        Updated testfile

    diff --git a/testfile b/testfile
    index e69de29..2cb609d 100644
    --- a/testfile
    +++ b/testfile
    @@ -0,0 +1 @@
    +Test Data
    ```
    As can be seen above, a line `Test Data` was added to testfile in the most recent commit.

    The stat option ( --stat ) shows abbrevieted stats for each commit.
    The online option ( --oneline ) shows commit history in single line.
    To view the commit graph, following command can be used -
    ```bash
    $ git log --graph --decorate --oneline --all
    ```

    To view commits grouped by author, following command can be used -
    ```bash
    $ git shortlog
    Akash (2):
        Initial Commit
        Updated testfile
    ```

    To view commits that affected a file/files, following command can be used -
    ```bash
    $ git log -- testfile # there is a space between -- and filename/s
    ```

    Git log outputs can also be limited based on time. Both --since and --until options are helpful.
    ```bash
    $ git log --since=2.weeks
    # more formats are supported
    # "2019-12-31" / "4 day 5 minutes aga" etc.
    ```

    Branch specific logs can be viewed by passing branch name to log command -
    ```bash
    $ git log branchname
    ```

    For more on log options visit [Git log documentation](https://git-scm.com/docs/git-log)


-   Using the diff command

    The diff command is useful in viewing changes between commits, working directory and staging area.
    ```bash
    $ git diff # changes between Working Directory and Staging Area
    $ git diff HEAD # changes between Working Directory and Last commit.
    $ git diff <commit-id> # changes between Working Directory and specified commit.
    $ git diff --staged # changes between staging area and Last commit. --cached option can also be used.
    $ git diff <commit-id-1> <commit-id-2> # changes between two commits
    ```


-   Ammending last commit
    The amend option ( --amend ) makes it possible to add any missed changes to be added to previous commit.
    ```bash
    $ git commit -m "Updated testfile"
    $ git add missed-file
    $ git commit --amend
    ```
    This replaces existing commit, so only one commit is recorded with requested changes.

-   Removing and Untracking a file from git
    If a file needs to be removed from Git project but is already added to staging area, following command can be used to remove it from both staging area and working directory.
    ```bash
    $ git rm filename # git cannot  remove a file if it is not already tracked.
    ```
    Simply removing a file from working directory ( by rm command or manually from file explorer) requires it to be removed from staging separately.
    Manual removal makes the file present in two sections of `git status` command.
    1. Changes to be commited - due to initial staging
    1. Changes not staged for commit - due to file deletion.

    The file can be removed from staging area using [reset command](#reset)

-   Moving files and Directories

    If files are renamed or moved from one directory to another, same issue occurs as above. Git does not track the file movement automatically.
    Git status shows that a file was deleted and a new file was added.

    So staging status will show a file as deleted, and untracked files will show a new file being added.

    To avoid this the mv command can be used.
    ```bash
    $ git mv testfile newfile
    ```

-   ### Branches

-   ##### View Branches
    To list existing branches, following command can be used -
    ```bash
    $ git branch
    $ git branch -v # lists existing branches along with their latest commit id.
    ```
-   ##### Create a new branch

    A new branch can be created in git with following command

    ```bash
    $ git branch newbranch
    $ git branch
    * master  # this is the current branch
    newbranch
    ```

    Work can continue on all branches independently.

    When a branch is created, both current and new branch point to the same commit in Git.

    ![Git Branch](./images/GitBranch.svg 'Git Branch Diagram')

-   ##### Switch ( Checkout ) to branch

    In git branching can be used to diverge from main development path to implement a new feature or modify existing code without affecting the master branch. Unlike most other VCS tools, branching and merging in Git is very fast and lightweight.

    To switch to new branch, following command can be used

    ```bash
    $ git checkout newbranch
    ```

    Alternatively, to create and switch to a new branch, following command can be used-

    ```bash
    $ git checkout -b newbranch
    Switched to a new branch 'newbranch'
    ```

-   ##### View differences in commits between different branches

    The following command shows the commits that are present in newbranch that are present in master branch

    ```bash
    $ git log master..newbranch
    commit dfa7acb60b6d32fcf3933b70f793e7a92c7213bb (HEAD -> newbranch)
    Author: Akash <akashvibhute@live.com>
    Date:   Thu Jan 2 21:19:24 2020 +0530

        added newfile
    ```

    The branch name positions can be altered to find changes that are done in master since the diversion.

    ```bash
    $ git log newbranch..master
    commit 0fc95828ab1a1c433444339a8d148b484016b9a7 (master)
    Author: Akash Vibhute <akashvibhute@live.com>
    Date:   Thu Jan 2 21:28:25 2020 +0530

        deleted missed-file and testfile
    ```

-   #### Merging Branches

    A branch can be merged with another branch. To merge a branch, switch/checkout to the branch in which another branch will be merged.
    The following command can then be used to complete the merge.

    ```bash
    $ git merge featurebranch
    ```

    There are two types of merges.

1.  Fast Forward Merge.

    When the master branch has no new commits since the new branch was created, a fast forward merge is done on merging.
    After this merge, the master branch points to the last commit on the merged branch.

    In fast forward merges, there should be no merge conflicts as the checked out (in this case - master) branch does not have any commits since the feature branch's diversion.

    ```bash
    $ git merge featurebranch
    Updating 0fc9582..df073c1
    Fast-forward
    featurefile | 0
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 featurefile
    ```

    _Notice the Fast-Forward in the output of above merge_

    The following diagram explains the fast forward merging process -

    ![Fast Forward Merge](./images/FastForwardMerge.svg 'Fast Forward Merge').

1.  Recursive Merge

    Recursive merging happens when the master branch has recent commits since the feature branch's diversion.
    This type of merge can also be forced in fast forward merge scenario by specifying --no-ff flag.
    This is useful if the branch needs to be merged in a new commit.

-   When a new commit is needed but fast forward merging would apply
    This merge can be also done with no fast forwarding. In that case, a new commit is created to which master branch points to.

    ```bash
    $ git merge featurebranch --no-ff
    Merge made by the 'recursive' strategy.
    featurefile2 | 0
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 featurefile2
    ```

    Running a log command shows the following output when a branch is merged recursively and then

    ```bash
    $ git log --oneline --graph --decorate --all
    *   f28b084 (HEAD -> master) Merge branch 'featurebranch'
    |\
    | * be3b0a5 Added another featurefile
    |/
    * df073c1 added featurefile
    * 0fc9582 deleted missed-file and testfile
    | * dfa7acb (newbranch) added newfile
    |/
    * f5e3525 Updated testfile
    * 51de56b Initial Commit
    ```

-   When master branch has recent commits after branch diversion-

    In this case there is a possibility conflicts occuring during merging if same file has been modified (and differs) in both branches. The conflicts need to be resolved manually.

    As seen in the above output, we have several commits on master branch after newbranch was created.

    On merging the newbranch with master, merge conflict occurs as newfile is changed in both the branches and git cannot automatically merge the file.

    ```bash
    $ git merge newbranch
    Auto-merging newfile
    CONFLICT (content): Merge conflict in newfile
    Automatic merge failed; fix conflicts and then commit the result.
    ```

    On checking the newfile, the conflicts can be seen as highlighted by git -

    ```diff
    Test Data
    <<<<<<< HEAD
    Master Change
    =======
    Newbranch Change
    >>>>>>> newbranch
    ```

    The changes below `<<<<< HEAD` and above `====` are from master branch. Once the changes are merged manually (the arrows need to be removed as well), the file can be commited and the merge is complete.

    The merged files need to be added to staging area before commiting.

    The following diagram shows the recursive merging -

    ![Recursive Merge](./images/RecursiveMerge.svg 'Recursive Merge')

    _Merges can be aborted in case of conflicts with following command -_

    ```bash
    $ git merge --abort
    ```

-   #### Deleting/Removing Branches

    The branches can be removed with following command-
    ```bash
    $ git branch -d branchname
    ```

    To forcefully remove branches that are not merged yet, following command can be used-
    ```bash
    git branch -D branchname
    ```
-   ### Rebase    
    Rebase allows us to change the base commit of a branch.

    In rebasing, the changes commited on one branch are implemented on another branch.

    ```bash
    $ git checkout branchname
    $ git rebase master
    ```

    Following diagram shows local rebasing done on a feature branch that was created when master branch was on M1 commit-
    ![Git Rebase](./images/rebase.svg "Git Rebase")




    When rebasing, the commit id of the branch will change.

    Rebasing is useful when working with remote and multiple contributors. When the branch on remote is updated by one users, others cannot push their changes to remote without merging the remote changes first. The pull command (used to get latest commits from remote) allows rebasing. The changes when pulled are directly merged recursively with current changes. When pulled with rebase, we can have a linear commit history.

    Rebasing with pull -
    ```bash
    $ git pull --rebase
    ```

-   ### Reset
    Resets are used to reset the current HEAD to specified state. It is also used to integrate changes from one branch into another. 
    
    1. Soft Resets-
        A soft reset moves the pointing of the HEAD to previous/specified commit (essentially discarding the latest commits) but leaves all the files in staging area. This is helpful when the commits need to be combined from a feature branch to master branch to avoid cluttering master history.
        ```bash
        $ git reset --soft <commit-id>
        ```
        Soft resets do not touch the working directory or the staging area.

    1. Mixed Resets-
    A mixed reset moves the pointing of HEAD to previous/mentioned commit but keeps the files in working directory. It clears the staging area. 
    This is useful when changes/files need to be commited in different commits.
    ```bash
    $ git reset --mixed <commit-id>
    $ git reset <commit-id>
    ```

    Mixed reset is the default option.

    To clear specific files from the staging area, following command can be used -
    ```git
    $ git reset HEAD filename
    ```
    Not specifying filename clears all files from staging area.

    1. Hard Resets - 
    Hard resets affect both staging area and working directory. It resets both to the specified commit state.

    To reset the working directory and staging area to latest commit, following command can be used- 
    ```bash
    $ git reset --hard HEAD
    ```

    Specifying a commit id instead of HEAD  resets the working directory/staging area to that commit's state -
    ```bash
    $ git reset --hard <commit-id>
    ```

    To push reset changes to remote, following command can be used -
    ```bash
    $ git push --force
    ```

-   ### Clean

    The untracked files can be removed from working directory with the clean command. To see the dry run of which files will be removed, `-n` option can be used-

    ```bash
    $ git clean -n # lists the files that will be removed
    Would remove tempfile
    ```

    To perform a clean on untracked files and directories, the `-df` option is used-
    ```bash
    $ git clean -df
    Removing tempfile
    ```

-   ### Revert

    To undo changes from past commit, revert is used. Revert will remove the changes made in the files by the commit and also removes any added files.
    ```bash
    $git revert <commit-id>
    [master eccc595] Revert "commit message"
    1 file changed, 0 insertions(+), 0 deletions(-)
    delete mode 100644 tempfile
    ```
    Reverts can also be reverted by  specifying the revert commit id in revert command.


-   ### Submodules 

-   #### Adding Submodules // This part is incomplete
    Submodules can be added to existing repository with following command -
    ```bash
    # git submodule add repository_url path_in_current_repository
    $ git submodule add https://gitlab.com/_akash/notes.git src/Notes/
    ```
    Adding a submodule creates a new `.submodules` file in the root directory of the current repository. It contains the submodule path and the url of the submodule repository.
    ```git
    [submodule "src/Notes"]
	path = src/Notes
	url = https://gitlab.com/_akash/notes.git
    ```
    
    The contents of the submodule are not pushed to remote. The submodule repository contains a link to the submodule repository and the commit hash -
    
    ![Git Submodules](./images/submodulerepresentation.png 'Notes Repository as submodule')

    By default cloning the parent repository will not clone the sumodules.
    
    To get the submodule content, following command can be used -

    ```bash
    $ git submodule init
    $ git submodule update # both the commands can be combined in a single command - `git submodule update --init`
    Cloning into 'D:/Projects/akashv/src/Notes'...                                            '
    Submodule path 'src/Notes': checked out '130a4219cd59fb6851cdee2e59eaf641da3b5791'
    ```
-   #### Updating submodule to latest commit

    submodules can be updated to latest commit by following command -
    ```bash
    $ git submodule update --remote --merge
    $ git commit # this will commit the submodule update in parent
    ```
    After the submodule is updated, the update has to be commited in parent repository.


-   #### Removing submodule

    Submodules can be removed with following commands-
    ```bash
    $ git submodule deinit -f -- submodule
    $ rm -rf .git/modules/submodule
    $ git rm -rf submodule
    ```
---

## References and Attributions

1.  <a href="https://git-scm.com/" target="_blank">Git SCM</a>
1.  <a href="https://git-scm.com/book/" target="_blank">Pro Git</a>
