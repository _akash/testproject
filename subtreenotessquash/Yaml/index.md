yaml needs space after semicolon to represent key: value pairs

lists are denoted as follows

```yaml
List:
    - item1
    - item2
    - item3
```

Dictionaries are represented as below

```yaml
Dictionary:
    Key: value
    key1: value
```

Equal no of spaces before all elements
for eg list of Dictionaries

```yaml
List:
    - Item1:
          key: value
          key2: value2
    - Item2:
          key3: value3
          key4: value4
```

Dictionaries are unordered, list is ordered.
line comments can be started with a #

```yaml
# this is a comments
```
