## Asynchronous Processing

**Parallel Processing-**

There are mechanisms in Pega to make Processes (Case with child case, Multiple assignments/flows in single case) seem like they are running in parallel from a user's perspective when in fact the system executes them sequentially.

Business Parallel Processing differs from Computational parallel processing.

With Business Parallel processing, two or more processes run within the same Java thread in which the processing is done alternatively.

**NOTE** - _The entire user session (Browser session) is a single requestor session. The BROWSER requestor is created on prpc url access. All requests from that session (browser window) use the same requestor object. Each requestor has associated temporary memory area on the server, which is known as clipboard. The threads represented on Clipboard are just instances of PRThread class which separates clipboard data of different threads. PRThread is not related to JVM thread but is a namespace used as a container of clipboard pages. It provides the context in which the user is making changes. For each new item the user is working on (case, rule etc) Pega creates a new PRThread instance along with various clipboard pages._

Computational Parallel Processing leverages multiple processing threads and other system resources simultaneously- this is true parallelism or asynchronous processing.

The Spinoff, Split Join and Split For Each mechanisms support Business Parallel Processing.

_In each of the above three mechanisms, each new case creation is done one after another, after which the flow in parent case moves ahead of the shape. These mechanisms are not truly asynchronous._

**Asynchronous Service Processing –**

True asynchronous processing in Pega is available with Asynchronous Service Requests. The EJB, HTTP, Java, JMS, MQ and SOAP services can leverage the agent queue functionality to process service requests asynchronously.

These services can be configured to run asynchronously or to only run the request asynchronously after a failure (with Queue When condition on Faults tab for soap). In both cases a queue item id (.pxQueueItemID) that identifies the queued request is returned in response. The results of this id can be retrieved later by calling different service.

When service is configured to run first attempt synchronously and other attempt asynchronously in case of failure, the queue item it is provided in a way that depends on the service type –

For HTTP, JMS and MQ services, Pega writes the queue item id in Parameter page of service.

NOTE- MQ - _IBM MQ is a proprietary message-oriented middleware product. JMS on the other hand is standard, not a specific implementation where each JMS provider can implement JMS their own way, as long as their implementation adheres to the JMS specification. IBM&#39;s JMS implementation is built on MQ. Therefore, in WebSphere, JMS is actually a wrapper for MQ. We usually use MQ when we need to integrate with a non-J2EE application that uses MQ. In rest of the scenarios JMS can be used._

For Java and EJB services, the service returns the exception of type PRQueuedRequestException that contains queue item id.

For SOAP services, the service returns SOAP fault with queue item id in Fault detail tag.

For Service SOAP rules, in both synchronous and asynchronous modes, two options are available-

Execute – Process the request and return the SOAP response

Execute (One way operation) – Process the request and only return the HTTP response code (2XX, 4XX etc)

For Service File Rules following two options are available in Asynchronous mode –

Execute (Queue For Execution) – With this option, the service queues the request with status "Immediate" and spawns a batch requestor in new thread on the same node. The queued items might not be processed in order when multiple threads are used. However requests within queued items will always be processed in order.

The batch size (records in each queue item) is configured in Requests per queue item field.

When processing method is set to file at a time, the maximum file size to be queued can be configured.

The service request processor can be configured to use several queue classes based on when conditions. Dequeuing options on service request processor define the retry limit and other persistance options.

Execute (Queue For Agent) – This puts the request in a queue with pyItemStatus set to "Scheduled" which is later picked up and processed by agent configured to process the queue.

Multithreaded file service needs to be tuned on case by case basis. On single node, queue for execution can be used to create multiple threads. For multi-node environments, queue for agent can be used.

The threadpool and the number of records per queue item can also affect performance.

The number of batch requestors (this is different from child requestors) determines the number of concurrent threads run by a batch requestor.

This is determined by default thread pool size on the web-tier. By default it is 5. This can be overridden by following entry in prconfig.xml –

```xml
<env name="agent/threadpoolsize "value="5" >;<!-- or the agent/threadpoolsize DSS. -->
```

For e-tier this setting is done on the container itself. For IBM WebSphere it is the Work Manager Pool Size, for weblogic it is the Number of concurrent MDBs (Message Driven Beans).

The throughput of File Service can be increased with the help of asynchronous processing. When processing method is selected as record at a time, queue item can be created with several single records. It is not possible to created queue items with several files.

**Asynchronous Connector Processing –**

Except Connect-File and Connect-FTP rules, all connectors can participate in asynchronous processing.

This is achieved by setting Execution mode to RunInParallel. This creates a child requestor in which connector is run. Connect-Wait method can be used to join child requestor session with current requestor session.

NOTE – Connect-Wait method has another parameter PoolID along with WaitSeconds. This pool id is used to manage asynchronous data loading in Load-DataPage and Call-Async-Activity methods.

// PoolID exists in the context of the activity using it, so same pool id can be used in different activities without any issues.

Call-Async-Activity method can only call activities that have Usage set to Asynchronous on security tab.

Such activities can only access two methods – Load-DataPage and Connect-Wait.

The results of Connect-Wait method depend on the values of both WaitSeconds and PoolID parameters. They can be found in help file of Connect-Wait method.

The parallel requestors are supported through a pool of limited requestors with default limit set to 10.

If the limit is reached, parent requestor fails. The limit can be adjusted in prconfig.xml or through Dynamic System Setting using "services/maxRequestorChildren". This is different from agent/threadpoolsize DSS.

The connect rules – (HTTP, SOAP and REST) offer one more execution mode- Queue.

In this case the connector request is sent to a queue for ProcessConnectQueue Agent to process.

// Note that this agent is different from ProcessServiceQueue which is used to process asynchronous service requests.

The queuing of the connection request is a database operation, which means that a Commit step is required after the Connect-\* step in the activity, or in the parent activity that calls this activity.

The Queued Connector operates in Fire and Forget style – There is no response available from the connector. The details of processing are configured on Connect Request Processor instance.

**Calling Activities Asynchronously-**

In Pega activities can be run asynchronously through Queue method. This is different from Call or Branch instruction such that the execution of this step occurs in Child Requestor and the calling activity does not need to wait/pause for the step execution to complete.

With Queue instruction the called activity does not receive a Step Page/ Primary page. Parameters can be passed through pass current parameter page option of the method. The parameter page is then shared between both activities. Parameters can also be passed through specific fields on method. This holds true for Call and Branch instructions as well.

The child requestor for execution is taken from limited pool of requestors controlled in prconfig.xml or agent/threadpoolsize DSS.

As this instruction is similar to Queue on few Connect- rules (see above), there is no need to use connect-wait.

**Parallelism through asynchronous processing can help improving end-to-end execution time, and result in greater overall utilization of computational resources. It can also introduce issues like locking, deadlocks, race conditions etc.**
