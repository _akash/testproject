#### Docker Engine

Docker engine consists of 3 components-

1. Docker Deamon - Backgroud process that manages images/containers/networks etc.
1. REST API - API interface to talk to the deamon.
1. Docker CLI - Command line interface to run actions on docker. It uses the REST API.

The docker cli can run commands on remote docker host with `-H` option as below -

```bash
$ docker -H=10.233.8.2:2375 run nginx
```

##### Containerization

The processes inside a container are processes running on the docker host, isolated by namespaces.
Inside the container, a process will have processid (PID) as 1, but on docker host,It will have a different processid as all processes have separate process id's. This provides isolation for the processes in each container and prevents the container from guessing PIDs of the docker host.

This behaviour can be overridden manually by adding `--pid host` option to the docker run/create command.

The create command only creates a container and does not run it. The run command creates and runs the container.


By default, there are no restrictions on resource utilization for containers.

The resource utilization limits can be set with following commands -

```bash
$ docker run --cpus=.5 --memory=50m nginx
```

#### Installing Docker -

-   ##### On Linux/Ubuntu -

    Installing using convenience script

    ```bash
    $ curl -fsSL https://get.docker.com -o get-docker.sh
    $ sudo sh get-docker.sh
    ```

-   ##### On Windows/Mac
    Download Docker Desktop from Docker site. For windows, 10 Pro or Enterprise Edition is required.

#### Docker commands

##### List running containers -

```bash
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                   NAMES
246bc9021729        jenkins/jenkins:lts   "/sbin/tini -- /usr/…"   19 hours ago        Up 19 hours         0.0.0.0:8080->8080/tcp  boring_murdock
```

##### List all containers on the system (including stopped/exited)

```bash
$ docker ps -a
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS                     PORTS                   NAMES
2d96584d4d02        hello-world           "/hello"                 4 seconds ago       Exited (0) 3 seconds ago                           boring_clarke
246bc9021729        jenkins/jenkins:lts   "/sbin/tini -- /usr/…"   19 hours ago        Up 19 hours                0.0.0.0:8080->8080/tcp  boring_murdock
```

##### List images on the system

```bash
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
jenkins/jenkins     lts                 a3f949e5ebfd        2 weeks ago         582MB
```

##### Stopping a container
```bash
$ docker stop container_id # or
$ docker stop container_name # both can be obtained from running above docker ps command
```
##### Remove a container

```bash
$ docker rm container_name
container_name # container name is printed back on successfully removing the container
```

##### Pull image

```bash
$ docker pull nginx:alpine # docker pull imagename
alpine: Pulling from library/nginx
89d9c30c1d48: Pull complete
24f1c4f0b2f4: Pull complete
Digest: sha256:0e61b143db3110f3b8ae29a67f107d5536b71a7c1f10afb14d4228711fc65a13
Status: Downloaded newer image for nginx:alpine
docker.io/library/nginx:alpine
```

##### List available images

```bash
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
jenkins/jenkins     lts                 a3f949e5ebfd        2 weeks ago         582MB
nginx               alpine              a624d888d69f        6 weeks ago         21.5MB
hello-world         latest              fce289e99eb9        12 months ago       1.84kB
```

##### Remove an image
```bash
$ docker rmi hello_world # docker rmi image_name
Untagged: hello-world:latest
Untagged: hello-world@sha256:d1668a9a1f5b42ed3f46b70b9cb7c88fd8bdc8a2d73509bb0041cf436018fbf5
Deleted: sha256:fce289e99eb9bca977dae136fbe2a82b6b7d4c372474c9235adc1741675f587e
Deleted: sha256:af0b15c8625bb1938f1d7b17081031f649fd14e6b233688eea3c5483994a66a3
```
##### Run Docker Container

```bash
$ docker run imangename
```

If Docker does not find the image locally, it will pull the imange from container registry (default is [Docker Hub](https://registry.hub.docker.com 'Docker Hub Registry')

##### Naming a container

It is important to name the containers to easily identify them. If a name is not provided, docker assigns a name to the container automatically.
Containers can be named with `--name` option

```bash
$ docker run --name=redis redis
```

##### Running commands on container

-   ###### While running the container

    ```bash
    $ docker run alpine ls #docker run image_name command
    bin
    dev
    etc
    home
    ...
    ```

    The container runs and executes the command. After the execution is complete, the process ends and container stops/exits.

-   ###### On A running Container

    The exec option can be used to execute a command on a running container

    ```bash
    $ docker exec boring_murdock  cat /var/jenkins_home/credentials/xml #docker exec container_name command
    <?xml version='1.1' encoding='UTF-8'?>
    <com.cloudbees.plugins.credentials.SystemCredentialsProvider plugin="credentials@2.3.0">
    <domainCredentialsMap class="hudson.util.CopyOnWriteMap$Hash">
        ...
    </domainCredentialsMap>
    ```

##### Attaching and Detaining Containers

By default containers run in Attached node, i.e. attached to the shell. The shell cannot be used until the container is stopped.

To run containers in detached mode, `-d` option is used along with run command.

To attach back to a run container, following command can be used-

```bash
$ docker attach container_id
```

To detach from a container press `ctrl + P + Q`

##### Run docker in interactive mode

```bash
$ docker run -i image_name
```

To attach with the container's terminal, use the -t option

```bash
$ docker run -it image_name
```

##### Port Mapping / Publishing

To map the ports on Docker container to Docker host, post mapping can be used.
The container are provided with local IP addresses that are only accesible on the Docker host.

To access from outside the docker host, port mapping is used.

The port mapping can be configured as below -

```bash
$ docker run -d -p 80:80 nginx # -p docker host port : docker container port
```

##### Volume Mapping

Docker containers have their own file systems stored in `/var/lib/docker/volumes/` directory. These live and die with the container. So, if the container exits/stops, the data inside is gone. To preserve the data, directories in the containers can be mapped to a directory on the docker host.

The volumes can be mounted as below -

```bash
docker run -v /hostdir:/containerdir image_name

```

If the entire path of the hostdir is not specified, a directory is craeteed in docker volumes (/var/lib/docker/volumes) with the hostdir name and the volume inside the docker is mounted on this directory.
This behaviour is same as shown in the [Jenkins Installation example](../Jenkins/Jenkins.md#install-with-docker 'Run Jenkis with Volume Mapping')

 ##### Docker Inspect

To get additional details of a docker container, inspect command is used. The `docker ps` command only gives limited results.

```bash
$ docker inspect container_name
```

Docker returns the container details in a json format.
Expand the box to view a sample inspect output for redis image can be viewed in [this file](./inspectredis.json 'Redis Inspect output')

##### Viewing Container logs

Container logs can be viewed with following command

```bash
$ docker logs container_id
```

##### Setting Environment Variables for Container

Environment variables can be set while running the container with following command-

```bash
$ docker run -e VAR_NAME=VAR_VALUE image_name
```

Environment variables from a running container can be found with `docker inspect container_id` command under Config in output json.

```json
"Config": {
    "Hostname": "2c0ec441ecc2",
    "Env": [
        "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
        "REDIS_VERSION=5.0.7",
        "REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-5.0.7.tar.gz",
        "REDIS_DOWNLOAD_SHA=61db74eabf6801f057fd24b590232f2f337d422280fd19486eca03be87d3a82b"
    ],
```

#### Linking containers-

In an application, some containers will be dependent on other containers to function properly.

Containers can be linked with the `--link` option which is _deprecated_ as below -

```bash
$ docker run -d --name=redis_alpine redis:alpine
$ docker run -d --name=App -p 8080:80 --link redis_alpine:redis # container_name:name_used_in_container
```

##### Recovering containers from failures-

Any docker container can be in one of the following four states-

1. Running

1. Paused

1. Restarting

1. Exited (also refers to stopped state)

Containers will keep on running as long as the command(s) it executes are still running. When the commands are complete, the container is stopped.
When the command(s) fails, the container exits.

To recover failed/exited containers, the `--restart` option can be used.

By default, the restart option is no, meaning no restarts are attempted.

To start the container on failures, following command can be used-

```bash
$ docker run -d --restart -on-failure[:max-retries] imagename

```

To always restart the container, but not if the container was previously put to stopped state manually, `-unless-stopped` can be used with `--restart`

To always restart the container when it is exited `-always` can be used with `--restart` option.

\_Docker uses backoff policy to avoid restarting containers too quickly. For each restart (assuming failures result in container exiting after restart), the time between attempted restarts keeps doubling.

##### Image Tags

To run an image with specific version, tags can be used. By default, if tag is not provided, latest is used.

###### Running an image without tag -

```bash
$ docker run redis # docker run image_name
Unable to find image 'redis:latest' locally
latest: Pulling from library/redis # notice the latest tag
8ec398bc0356: Already exists
da01136793fa: Pull complete
cf1486a2c0b8: Pull complete
a44f7da98d9e: Pull complete
c677fde73875: Pull complete
727f8da63ac2: Pull complete
Digest: sha256:90d44d431229683cadd75274e6fcb22c3e0396d149a8f8b7da9925021ee75c30
Status: Downloaded newer image for redis:latest
...
1:M 05 Jan 2020 10:41:22.878 * Ready to accept connections
```

Notice that the image with latest tag was pulled to run the container.

######   Running a specific image

If a specific image is required, the tag can be specified -

```bash
$ docker run redis:alpine
Unable to find image 'redis:alpine' locally
alpine: Pulling from library/redis
e6b0cf9c0882: Already exists
7c5ff11edca6: Pull complete
14fa80ee9473: Pull complete
4d4f6840431a: Pull complete
9d4162ad1104: Pull complete
b2c320096d0f: Pull complete
Digest: sha256:a4e0b7bff7ecec0dc0be95d185d6c99323a92a51065d9563a5bafbc1cf6b3497
Status: Downloaded newer image for redis:alpine
...
1:M 05 Jan 2020 10:46:58.170 * Ready to accept connections
```

Docker pulls the specified image and runs it. The current available images should have both of these listed-

```bash
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
redis               alpine              8af6a13284bf        2 days ago          33MB
redis               latest              9b188f5fb1e6        2 days ago          98.2MB
nginx               latest              f7bb5701a33c        7 days ago          126MB
alpine              latest              cc0abc535e36        11 days ago         5.59MB
jenkins/jenkins     lts                 a3f949e5ebfd        2 weeks ago         582MB
nginx               alpine              a624d888d69f        6 weeks ago         21.5MB
```

Both the images are listed with their tags.

For public images, the tags can be found on the image page on [Docker Hub](https://hub.docker.com 'Docker Hub')

#####   Saving an image to disk

Images built by docker can be stored locally and transferred to other devices. To store an image following command is used-

```bash
$ docker save -o filename.tar imagename
```

#####   Loading a saved image

Saved images from disk can be used to run containers. They can be loaded with following command-

```bash
$ docker load -i filename.tar
```

#### Docker Images

##### Creating a new Image -
Docker images can be created with Dockerfiles. `Dockerfile` contains the instructions to build the docker image.

#### Dockerfile

Dockerfile are defined in an INSTRUCTION argument format.
So, the first word in line is an instruction for docker to run, and the rest of the line is an argument(s) for that instruction.

The starting point of dockerfile is the FROM keyword, stating the image that this perticular docker imgage will be/is based on.

```docker
FROM node:12.4
```

Setting LABELS

```docker
LABEL maintainer "email"
```

Healthcheck Command to check if the container is running or not

```docker
HEALTHCHECK --interval=5s --timeout=5s CMD curl -f http://127.0.0.1:8000 ||exit 1
```

Setting Work Directory

```docker
WORKDIR /usr/src/application
```

Copying source code

```Docker
COPY /src ./ # copy Everything under /src to ./ on the image file system
```

Run commands

```docker
RUN npm install # example for node.js project, adduser ... to add new user etc.
```

Exposing ports

```docker
EXPOSE 8000
```

Docker Command to run the application packaged in the image -

```docker
CMD ["npm","start"] # for starting a node js app (start command needs to be defined in package.json)
```

The CMD command takes an array as an argument or a normal command string -
`CMD command arg1` or `CMD ["command","arg1"] are same.

Dockerfile commands are ignored if command is passed when running the container

```docker
$ docker run imagename command args # will ignore anything defined in the CMD instruction in dockerfile.
```

**Docker Command (_CMD_) vs Entrypoint (_ENTRYPOINT_)**

With CMD argument, the command passed when starting the container replaces the CMD arguments.

With ENTRYPOINT instruction, the command passed when starting the container gets appended to the ENTRYPOINT arguments.

```docker
ENTRYPOINT ["command"]
```

Both the ENTRYPOINT and CMD can be used together.

The command to run should be specified in the ENTRYPOINT instruction and its arguments in the CMD instruction.

If any commands/parameters are passed when running the container, they are appended to the ENTRYPOINT and CMD gets ignored.
If no commands/parameters are passed when runnint the container, the CMD arguments get appended to the ENTRYPOINT command and container runs in its base config.

The ENTRYPOINT can be overriden while running the container with `--entrypoint` option-

```bash
$ docker run --entrypoint new_command imagename
```


###### Ignoring files/directories while copying them to image

Some files/directories like node_modules for nodejs are not required to be part of the image and can reduce the size
if not included in the image.

Similar to `.gitignore` files in git, files and folders can be ignored in docker with a `.dockerignore` file.

##### Building image from Dockerfile

Docker images can be created by running the following command -

```bash
$ docker build -t username/imagename:tag .
```


Docker images are created in a layered architecture. For example, a sample dockerfile build (with some commands as above) would output like -

Dockerfile used -
```dockerfile
FROM node:alpine
LABEL maintainer "myid"
EXPOSE 8000
CMD ["sleep","1000"]
```

Build output -

The base image for this dockerfile is node:alpine. The node image is based on alpine image which already exists in the system so docker does not pull it again.

For each for steps in the dockerfile, docker builds a new image and then builds the next line on top of previous image.
Between the builds docker removes the previous builds once they are used. This can be amended (to keep intermediate builds as well) with `--rm`` flag, which is true by default.

```bash
root@rc:~# docker build -t akash/testimage .
Sending build context to Docker daemon  30.72kB
Step 1/4 : FROM node:alpine
alpine: Pulling from library/node
e6b0cf9c0882: Already exists
ab436df1df6f: Pull complete
470300a8a365: Pull complete
84e7c11579ee: Pull complete
Digest: sha256:b3f6a315aedc31ef3958108ce4926a52b4b4bcc93fca9655737d057de54f8157
Status: Downloaded newer image for node:alpine
---> e1495e4ac50d
Step 2/4 : LABEL maintainer "myid"
---> Running in 2e05f630abf4
Removing intermediate container 2e05f630abf4
---> 4f36912c81db
Step 3/4 : EXPOSE 8000
---> Running in 746de439ff2a
Removing intermediate container 746de439ff2a
---> 0fe6812e12f0
Step 4/4 : CMD ["sleep","1000"]
---> Running in 5d77361a623a
Removing intermediate container 5d77361a623a
---> 30d94e865fbd
Successfully built 30d94e865fbd
Successfully tagged akash/testimage:latest
```

Docker also caches the layers, so rebuilds of the same image will not require building the whole layer, only ones that have changed.

##### Pushing image to docker registry
###### by default it goes to docker hug registry. Login is required which can be done with docker login command in advance.
```bash
$ docker push imagename:tag 
```

#### Networking in Docker

When docker is installed, 3 networks are created -

1.  ##### Bridge -

    This is a private internal network created by docker on the docker host. All containers in this network get an IP address assigned to them (usually in 172.17.0.0 subnet).

    The containers in this network can access each other by this IP.

    To access containers in this network from outside the network, ports can be mapped to docker host's ports as seen in [Port Mapping](#port-mapping 'Port Mapping') section.

    This is the default network that a container gets attached to.


1.  ##### None -

    In this, the containers are not attached to any network and cannot be accessed externally or by other containers.

    To attach a container to this network, following command can be used-
    ```bash
    $ docker run image-name --network=none
    ```
1.  ##### Host -

    In this network type, the containers are directly associated with Host network. Port mapping is not required in this case.

    In this mapping, the same image cannot be used to run multiple containers as the Host port can be used by only one container.

    To attach a container to this network, following command can be used-
    ```bash
    $ docker run image-name --network=host
    ```

Internal networks can be created manually to isolate groups of containers to a network on a docker host.

##### View existing networks on docker host
Following command can be used to list existing networks on the docker host -
```bash
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
f07dd35f0a33        bridge              bridge              local
cb21b94f51e6        host                host                local
dc440ac9a8a6        none                null                local
```
##### Creating a network

Networks can be created by specifying the driver and subnet with the `network create` command. On creation, docker outputs the network id -
```bash
$docker network create --driver bridge --subnet 182.18.0.0/16 network-name
55e98b00427b41603ac7c6b331f0783a7dc28e7a7af302d5d24c28c3e02a4c76
$ docker network ls # notice the new network added with the new network id and driver.
NETWORK ID          NAME                DRIVER              SCOPE
f07dd35f0a33        bridge              bridge              local
cb21b94f51e6        host                host                local
55e98b00427b        new-network         bridge              local
dc440ac9a8a6        none                null                local
```

A container's network setting can be viewed with the `docker inspect` command.


Containers can also access each other with container names. Docker's built in DNS server resolves the container names to  their IP address.

#### Docker storage

Docker stores all the data on local file system at `/var/lib/docker`

There are specific directories under above directory for containers, images and volumes.

##### Creating volumes-

To persist the data after container exits, volumes can be added to the container.
To create a volume following command is used -

```bash
$ docker volume create newvolume
newvolume
```

This creates a new directory under `/var/lib/docker/volumes` called `newvolume` -

```bash
$ ls /var/lib/docker/volumes/ -la
drwxr-xr-x  3 root root  4096 Jan  5 20:18 newvolume
```

**Volume Mounting**

This newly create volume can be mounted inside a container to a directly with following command -

```bash
$ docker run -v newvolume:/var/lib/redis redis
```

If the volume does not exist already when running above command, docker will create the volume before running the image.

**Bind Mounting** -

If a path is specified in above command instead of volumename, the path on docker host is mounted in the container on specified directory.

```bash
$ docker run -v /data/redis:/var/lib/redis redis
```

The storage mounting can also be done with the `--mount` option as below -

```bash
$ docker run --mount  type=bind,source=/data/redis,target=/var/lib/redis redis
```

This is the preferred way of mounting the storage.

Docker uses storage drivers to enable the layered architecture. Docker chooses the storage drivers automatically based on the host operating system. For ubuntu it is AUFS.

#### Docker Compose

Running and managing individual container for an application manually is tedious and can be better done with docker compose.

Docker compose uses [yaml](../Yaml/yaml.md 'YAML') files in a `docker-compose.yml` file to create the configuration file to put together all the services and their options.

Docker compose can be installed on linux with package manager or python pip -

```bash
$apt install docker-compose # or
$ pip install docker-compose
```

All the services can then be started with a single command -

```bash
$ docker-compose up -d
```

Each container/image is listed in the docker-compose.yml file as a list element

```yaml
container1:
    image: image1
container2:
    image: image2
    ports:
        - 80:80
    links:
        - container:linkname
```

To build the images from a docker-compose.yml file, the image part can be replaced with a directory for the Dockerfile -

```yaml
container1:
    build: ./appdir
```

When running `docker compose up` with above docker compose file, docker will first build the images and then run them instead of trying to pull them from the registry.

##### Versions of Docker Compose

The version of the file needs to be mentioned in compose file at the top of the file -

```yaml
version: 2
services:
    container1:
        image: image1
    container2:
        image: image2
```

With the version 2 of the docker file, all the container/service descriptions are moved to services: dictionary in the docker-compose.yml

With version 2, docker creates a new bridged network for the services and attaches all the services to that network. So links are not required as all containers can communicate with each other with their service names.


##### Managing container interdependencies -

Sometimes, containers need to be started in a specific order.
With version 2, docker has a depends on feature to manage the dependencies -

```yaml
version:2
services:
    container1:
        image: image1
    container2:
        image: image2
        depends_on:
            - container1
```

With version 3 of the docker compose, support for docker swarm was added.

With version 2, containers can also be attached to different networks -
```yaml
version:2
services:
    container1:
        image: image1
        networks:
            - network1
            - network2
    container2:
        image: image1
        networks:
            - network2
networks:
    network1:
    network2:
```

#### Docker Registry

Docker images are stored in a docker registry. The default registry as mentioned is the [Docker Hub](https://registry.hub.docker.com 'Docker Hub Registry').

In the registry, the images are stored as username:imagename (imagename being same as repository name)

if the username is not provided, docker assumes it is same as imagename -
`nginx` is same as `nginx:nginx`

##### Hosting private registries -

Docker currently allows only 1 private repository and unlimited public repositories on the docker hub registry.

To keep the docker images private, a private registry can be created.

The docker registry itself is a docker image, which can be run on the docker host to host private images.

```bash
$ docker run -d -p 5000:5000 --name private-registry registry:2
```

##### Pushing images to private registry -

Images can be pushed to the private registry by tagging them with the registry url

```bash
$ docker image tag image-name localhost:5000/image-name
```

##### Logging in to private registry -

```bash
$ docker login localhost:5000
```

##### Running images from private registry -
```bash
$ docker run localhost:5000/image-name
```

##### Pulling images from private registry -
```bash
$ docker pull localhost:5000/image-name # on same docker host
```
