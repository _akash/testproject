## SSH is short for Secure Shell

ssh is used for secure remote access from one machine to another. It can be used for file transfers, issuing remote commands and managing network infrastructure.
Read more about ssh on [ssh.com](https://ssh.com/ssh/protocol 'ssh protocol') and about ssh encryption/connection process on [DigitalOcean Tutorial](https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process 'Digital Ocean Tutorial')

The ssh keys are a pair of public and private keys. The public key resides on the system that is being accessed remotely, and the private keys reside on user's personal machine (typically under user/.ssh folder)

### Generating SSH Keys

-   #### With ssh-keygen

    The ssh-keygen command can be used to generate ssh keys without any arguments. It will ask for filename to store the key in, the default is user/.ssh/id_rsa for rsa algorithm and would change based on algorithm used.
    A passphrase can be entered for additional security, so the private key cannot be used without providing passphrase.

    The algorithms to use can be defined with -t option and key size with -b option.

    The filename prompt can be avoided with a -f {filepath} argument. So an example with all options will look like -


    ```bash
    ssh-keygen -f ~/my-key-ecdsa -t ecdsa -b 521
    ```
    ![ssh-keygen](./images/ssh-keygen.png)

-   #### with PuTTYgen (PuTTY Key Generator)

    PuTTYgen provides a nice user interface for generating ssk keys.
    It allows you to select the number of bits and the algorith for the keys. Clicking on generate and providing random input by mouse movement will generate the key. A passphrase can be provided if required.

    ![PuttyGen](./images/putty-keygen.png)
    It then allows you to save the generated public/private keys.
    Private keys can be stored in .ppk file format or can be exported to OpenSSH format (also ssh.com format).

    ![PuttyGenExport](./images/putty-keygen-export.png)

### Using ssh

-   #### Remote login

-   #### Accessing remote Git repo

    All git repository hosting services allow to add public keys to your profile to bypass password authentication when accessing your remote repo from local system.

    ##### 1. GitHub

    The public key can be added to profile by visiting [SSH and GPG keys](https://github.com/settings/keys 'SSH and GPG keys') page on Github profile and clicking New SSH key button or by directly visiting [Add new SSH keys](https://github.com/settings/ssh/new 'Add new SSH keys') page. The [SSH and GPG keys](https://github.com/settings/keys 'SSH and GPG keys') page lists existing keys associated with your account. The keys can be deleted from this page.

    After adding the key, the connection can be tested by running following command.

    ```bash
    ssh -T git@github.com
    ```

    ![Github Connection Test](./images/github-test.png 'Github Connection Test')

    For first time connection with github, the authenticity of github host needs to be verified. Answering yes adds github.com to the list of trusted hosts.

    ##### 2. Gitlab

    The public key can be added to profile by visiting [SSH Keys](https://gitlab.com/profile/keys 'SSH Keys') page on gitlab.

    If you have gitlab running on your own host, access the SSH Keys page from https://\<hostname>/profile/keys url.

    After adding the key, the connection can be tested by running following command.

    ```bash
    ssh -T git@gitlab.com  # or replace gitlab.com with your gitlab host instance domain.
    ```

    ![Gitlab Connection Test](./images/gitlab-test.png 'Gitlab Connection Test')

    For first time connection with gitlab, the authenticity of gitlab host needs to be verified. Answering yes adds gitlab.com | gitlab host domain to the list of trusted hosts.

    #### 3. Bitbucket

    The public key can be added to bitbucket by visiting [account settings](https://bitbucket.org/account/user/<Username>/ 'account settings') page. If the bitbucket instance is running on private server, replace bitbucket.org with your host's domain.
    The _SSH Keys_ link under _SECURITY_ section provides access to ssh keys page.

    After adding the key, the connection can be tested by running following command.

    ```bash
    ssh -T git@bitbucket.org  # or replace bitbucket.org with your bitbucket host instance domain.
    ```

    ![Bitbucket Connection Test](./images/bitbucket-test.png 'Bitbucket Connection Test')
