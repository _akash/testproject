### Redis

Redis is an open source _in memory_ data structure store. Redis can be used as database, cache and message broker.
It is a key - value data store.

Redis uses built-in persistance, so data is safe even with restarts.

#### Installing Redis -

-   On Linux

    Redis can be installed with package managers on linux distributions.

    ```bash
    $ apt install redis
    ```

#### Data Types

-   Strings
-   Hashes
-   Lists
-   Sets
-   Sorted Sets
-   Bitmaps
-   Hyperlogs
-   Geospatial
-   Indexes

Redis can be scaled easily when used as cache. With partitioning, data can be split across multiple redis instances.

#### Security

Redis is designed to be accessed by trusted clients inside trusted environments. It should not be exposed to public access. Redis is optimized more for performance than security.

#### Redis clients

Redis provides clients for most of the programming languages for ease of use.
