# review official documentation here
https://git-scm.com/docs/


# initializing repo
git init

# config for user and all //global/local/system
# get existing values
git config --global --list

# set values
git config --global user.name "Akash Vibhute"
git config --global user.email "akashvibhute7@gmail.com"

# edit config file
git config --global --edit

# recording file changes
# current project state, shows branch and commit along with untracked/modified/deleted files
git status

# add files to project 
git add filename  	# Add filename to staging area
git add -A 			# Add all new files, modification and deletions to staging area
git add .				# Add New and Modified files to staging
git add -u			# Add Modified and Deleted files to staging


# commit - a snapshot of project files at a given point
# files can be recovered from a past commit
# commit with a commit message with -m parameter, or just commit will open a text editor to add test
git commit -m "Initial commmit" # multiple -m "" can be used

# commit updates in single command
git commit -am  "single commit"

# view commit log
git log
git log --stat -n # stats commit log for recent n commits
git log --patch  # detailed commit log
git shortlog # commits per user
git log -- Ansible.sh # commits for perticular file

# removing and untracking a file from git repo
# removing file from terminal/file manager requires deletion staging separately
git rm file.txt
# Using the diff command:
#Working <=> Staging
git diff

#Working <=> commit
git diff HEAD	 #or below
git diff <most-recent-commitref> #commit id

# Staging <=> Commit
git diff --staged	# or git diff --cached

#Two commits
git diff  a6145  c2722

# Branches
# List existing branches:
git branch

# List existing branches in the git repository with the most recent commit on each one
git branch -v

# Create a new branch in the repository
git branch branch-name

#Switch to a branch
git checkout branch-name

# Create a new branch And switch to it
git checkout -b branch-name

# view commits on branches
git log branch-name --oneline

# View all the commits accross different branches in the project
git log --oneline --graph --decorate --all

# View difference in terms of commits between different branches
git log branch1..branch2 --oneline
# The above command displays commits on branch2 that are not yet merged with branch1.  branch1 and branch2 could be either master or feature.

# branch merging - two types of merging - Fast forward merging and recursive merging
# for fast forward merging, there are no new commits on the master branch since current branch's diversion from master. 
# when merged, head for master points to recent commit on the current branch.
# there should be no merge conflicts. 
# a new commit can be added with fast forward merge.

# switch to master branch
git checkout master
# merge branch
git merge branchname_ff


# recursive merging
# this merging occurs when the destination (master) branch is moved ahead after diversion.
# checkout to master and merge -
git merge branchname_rc

# instead of recursive merge, a rebase can be performed.

# git status will show what can be done to resolve the merge conflicts

# merges can be reverted in case of conflicts
git merge --abort

# forcing a new commit on fast forward merge by specifying a no fast forward flag with merge command
git merge feature1 --no-ff

# removing branches -
# list merged branches
git branch --merged

# branches not merged with current branch
git branch --no-merged

# Remove merged branches 
git branch -d branchname

# remove branch forcefully (even unmerged branches will be removed)
git branch -D branchname

# remote repo - 
# create a git project on remote first
# add remote 
git remote add origin git@gitlab.com:_akash/notes.git

# -u flag is not necessary
# push a branch 
git push -u origin master 

# push all
git push -u origin -all 


# push only --tags
git push -u origin --tags


# tagging commits 
# a tag and commits have one to one relationship
# list all tags
git tag 

# tags most recent commit 
git tag v1.0 

# tag specific commit
git tag v2.0 6868a5faf59a74d4c8aba8cecb41e95b1735af6a  # commit id ( need not be complete id, first few characters are sufficient)

# list tag commit details
git log v1.0 # specify tag name

# for more details of a commit, use show command
git show v1.0 

# adding tag annotations
# annotations provided with two ways -a and -m
git tag -a release/1.0.0 -m "1.0.0 release"


# pushing tags to remote

git push --follow-tags
# git can be configured to set follow tags automatically
git config --global push.followTags true

# remove tags 
git tag -d v1.0

# remove remote tags
git push --delete origin tagname

# resets # should not be used on master/main branch
# 3 types of resets
# soft resets - combine changes from multiple commits in a branch

git reset --soft <commit-id> # discords other commits, but keeps files in staging area, so a commit needs to be performed.

# soft resets do not touch the working directory or the staging area.

# mixed resets - does not touch the working directory, but clears the staging area. 
# the files in working directory need to be staged manually. 
# this is useful if files/changes need to be commited in separate commits.  
# mixed reset to a past commit
git reset --mixed  <commit-id>
# or without --mixed,  as --mixed is default
git reset <commit-id>

# clears all the files in staging
git reset HEAD

# clear/unstage a specific file from staging e.g. README.md
git reset HEAD readme.md

Hard reset to a past commit
>>	git reset --hard past-commit-id

# hard resets - affect both staging and working directory, resets both to the commit state
# hard reset to the most recent commit
git reset --hard HEAD

# hard reset to specific commit
git reset --hard <commit-id>

# to reset remote 
git push --force

# clean
# dry run of git clean to see which files will be removed
git clean -n

# Perform a clean on untracked files and directories (useful for removing files from a mixed reset)
git clean -df


# Git revert - undo changes from past commit to a new commit
git revert <commit-id> # removes specific commit changes from a file, also removes added file.

# reverting a revert -
# sane cinnabd as above, with commit id of the revert"

# reflogs (referenced logs - useful for recovering files from deleted branches/commits)
# shows commit history
git reflog

# the use git checkout with commit id to switch to that working directory
git checkout <commit-id>

# stash
# When switching from branches/commits, uncommited changes are carried over.
# stashing saves uncommited changes saves changes as WIP on the branch and cleans working directory
git stash

# show list of stashes
git stash list

# apply stash
git stash apply stash@{n} # n is number from above list

# apply recent stash
git stash apply 

# stashes can be created and one branch and applied on different branch
# push is not necessary
git stash push --include-untracked -m "stash description"

# use show to view stash details
git stash show show | show@{stash_no}

# apply changes from staging area
git stash apply --index

# apply a stash and remove from stash list
git stash pop --index

# clear all stashes
git stash clear

# Detailed commands

# checkout command
# checkout to new/existing branch / past commit|tag / recover files
git checkout <commit-id>|<tag>

# recovering a file from staging area
git checkout -- filename

# recovering/resetting a file from recent commit
git checkout HEAD -- filename


# remotes
# setup ssh keys as detailed in git part of ssh-access.sh
# some commands are explained above at specific places
# new remote
git remote add origin <git_url>

# view remote url
git remote -v

# rename remote
git remote rename origin new-name

# push 
git push origin branch-name # --all for all branches
# a --set-upstream | -u option can be specified for git to remeber the remote as default

# pull
git pull origin

# remove
git remote rm origin

# cloning a repo-
git clone <git-url>

# git creates remote tracking branches on local system for tracking push/pull
# list remote tracking branches
git branch -r # ( --all for all branches )

# list local and remote branches and display their relation
git branch -vv

# view detailed information about remote tracking branches configured for the origin remote
git remote show origin

# fetch recent commits from remote to remote tracking branch 
# manual  merge is required to master branch from remote tracking branch
git fetch

# pull (stash/commit changes prior to running pull)
git pull origin master # remote branch name is not necessary

# merge conflicts in pull need to be resolved manually before pushing new changes

# rebase - change base commits of branches 
# checkout to branch
git rebase master
# do not rebase a branch if it is pushed to remote
# use abort to completely undo rebase 
git rebase --abort

Remember credentials in cache memory for upto 8 hours ( 60 * 60 * 8 ) after the first successful authentication
>>	git config --global credential.helper 'cache --timeout 28800'

Erase credential cache immediately to clear the username and password from cache memory
>>	git credentential-cache exit

Store credentials in a file after the first successful authentication
>>	git config --global credential.helper 'store --file ~/.mycredentials'

Read the contents of the git configuration file for the current user
>>	git config --global --list

Edit the contents of the git configuration file for the current user
>>	git config --global --edit
